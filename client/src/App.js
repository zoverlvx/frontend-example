import React from "react";
import "./styles/App.scss";

function App() {
  return (
    <div className="container">
      <header>
          <nav>
              <div>Logo</div>
              <ul>
                  <li>Home</li>
                  <li>About</li>
                  <li>Products</li>
                  <li>Help</li>
              </ul>
          </nav>
          <h1>
              This is a headline
          </h1>
      </header>
    </div>
  );
}

export default App;
